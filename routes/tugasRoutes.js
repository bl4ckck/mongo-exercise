var express = require('express');
var router = express.Router();
var tugasController = require('../controllers/tugasController.js');

/*
 * GET
 */
router.get('/', tugasController.list);

/*
 * GET Leaderboard
 */
router.get('/leaderboard', tugasController.list);

/*
 * GET
 */
router.get('/:id', tugasController.show);

/*
 * POST
 */
router.post('/', tugasController.create);

/*
 * PUT
 */
router.put('/:id', tugasController.update);

/*
 * DELETE
 */
router.delete('/:id', tugasController.remove);

module.exports = router;
