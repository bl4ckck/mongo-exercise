const express = require('express');
const router = express.Router();

const siswa = require('./siswaRoutes');
const tugas = require('./tugasRoutes');

var siswaController = require("../controllers/siswaController.js");


router.use('/siswa', siswa);
router.use('/tugas', tugas);
router.use("/leaderboard", siswaController.listLeaderboard);

module.exports = router;