var express = require('express');
var router = express.Router();
var siswaController = require('../controllers/siswaController.js');

/*
 * GET
 */
router.get('/', siswaController.list);

/*
 * GET List Tugas
 */
router.get('/tugas', siswaController.listTugas);

/*
 * GET
 */
router.get('/:id', siswaController.show);

/*
 * POST
 */
router.post('/', siswaController.create);

/*
 * PUT
 */
router.put('/:id', siswaController.update);

/*
 * DELETE
 */
router.delete('/:id', siswaController.remove);

module.exports = router;
