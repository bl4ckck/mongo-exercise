const express = require("express");
const app = express();

const api = require("./routes");
require("./db");

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/api/v1", api);

const port = process.env.PORT || 5000;
app.listen(port, () => {
    /* eslint-disable no-console */
    console.log(`Listening: http://localhost:${port}`);
    /* eslint-enable no-console */
});

module.exports = app;
