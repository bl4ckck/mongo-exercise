var TugasModel = require('../models/tugasModel.js');

/**
 * tugasController.js
 *
 * @description :: Server-side logic for managing tugass.
 */
module.exports = {
    /**
     * tugasController.list()
     */
    list: (req, res) => {
        TugasModel.find((err, tugass) => {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting tugas.',
                    error: err
                });
            }

            return res.json(tugass);
        });
    },
    /**
     * tugasController.show()
     */
    show: (req, res) => {
        var id = req.params.id;

        TugasModel.findOne({_id: id}, (err, tugas) => {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting tugas.',
                    error: err
                });
            }

            if (!tugas) {
                return res.status(404).json({
                    message: 'No such tugas'
                });
            }

            return res.json(tugas);
        });
    },

    /**
     * tugasController.create()
     */
    create: (req, res) => {
        var tugas = new TugasModel({
			siswa : req.body.siswa,
			poin : req.body.poin
        });

        console.log("request =" + JSON.stringify(req.body.poin));
        tugas.save((err, tugas) => {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating tugas',
                    error: err
                });
            }

            return res.status(201).json(tugas);
        });
    },

    /**
     * tugasController.update()
     */
    update: (req, res) => {
        var id = req.params.id;

        TugasModel.findOne({_id: id}, (err, tugas) => {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting tugas',
                    error: err
                });
            }

            if (!tugas) {
                return res.status(404).json({
                    message: 'No such tugas'
                });
            }

            tugas.siswa = req.body.siswa ? req.body.siswa : tugas.siswa;
			tugas.poin = req.body.poin ? req.body.poin : tugas.poin;
			
            tugas.save((err, tugas) => {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating tugas.',
                        error: err
                    });
                }

                return res.json(tugas);
            });
        });
    },

    /**
     * tugasController.remove()
     */
    remove: (req, res) => {
        var id = req.params.id;

        TugasModel.findByIdAndRemove(id, (err, tugas) => {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the tugas.',
                    error: err
                });
            }

            return res.status(204).json();
        });
    }
};
