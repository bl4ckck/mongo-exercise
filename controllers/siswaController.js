var SiswaModel = require('../models/siswaModel.js');

/**
 * siswaController.js
 *
 * @description :: Server-side logic for managing siswas.
 */
module.exports = {
    /**
     * siswaController.list()
     */
    list: (req, res) => {
        SiswaModel.find((err, siswas) => {
            if (err)
                return res.status(500).json({
                    message: "Error when getting siswa.",
                    error: err,
                });

            return res.json(siswas);
        });
    },

    /**
     * tugasController.listTugas()
     */
    listTugas: async (req, res) => {
        const tugasAgg = await SiswaModel.aggregate([
            {
                $lookup: {
                    from: "tugas",
                    localField: "_id",
                    foreignField: "siswa",
                    as: "tugas",
                },
            },
            // { $group: { _id: null, maxBalance: { $max: "$balance" } } },
            // { $project: { _id: 0, maxBalance: 1 } },
        ]);

        return res.json(tugasAgg);
    },

    /**
     * tugasController.listLeaderboard()
     */
    listLeaderboard: async (req, res) => {
        const leaderboard = await SiswaModel.aggregate([
            {
                $lookup: {
                    from: "tugas",
                    localField: "_id",
                    foreignField: "siswa",
                    as: "tugas",
                },
            },
            { $unwind: "$tugas" },
            {
                $group: {
                    _id: "$nama_depan",
                    user: { $first: "$$ROOT" },
                    poin: { $avg: "$tugas.poin" },
                },
            },
            {
                $replaceRoot: {
                    newRoot: { $mergeObjects: ["$user", { poin: "$poin" }] },
                },
            },
            { $project: { tugas: 0 } },
            { $sort: { poin: -1 } },
        ]);

        return res.json(leaderboard);
    },

    /**
     * siswaController.show()
     */
    show: (req, res) => {
        var id = req.params.id;

        SiswaModel.findOne({ _id: id }, (err, siswa) => {
            if (err) {
                return res.status(500).json({
                    message: "Error when getting siswa.",
                    error: err,
                });
            }

            if (!siswa) {
                return res.status(404).json({
                    message: "No such siswa",
                });
            }

            return res.json(siswa);
        });
    },

    /**
     * siswaController.create()
     */
    create: (req, res) => {
        var siswa = new SiswaModel({
            nama_depan: req.body.nama_depan,
            nama_belakang: req.body.nama_belakang,
            foto: req.body.foto,
        });

        siswa.save((err, siswa) => {
            if (err) {
                return res.status(500).json({
                    message: "Error when creating siswa",
                    error: err,
                });
            }

            return res.status(201).json(siswa);
        });
    },

    /**
     * siswaController.update()
     */
    update: (req, res) => {
        var id = req.params.id;

        SiswaModel.findOne({ _id: id }, (err, siswa) => {
            if (err) {
                return res.status(500).json({
                    message: "Error when getting siswa",
                    error: err,
                });
            }

            if (!siswa) {
                return res.status(404).json({
                    message: "No such siswa",
                });
            }

            siswa.nama_depan = req.body.nama_depan
                ? req.body.nama_depan
                : siswa.nama_depan;
            siswa.nama_belakang = req.body.nama_belakang
                ? req.body.nama_belakang
                : siswa.nama_belakang;
            siswa.foto = req.body.foto ? req.body.foto : siswa.foto;

            siswa.save((err, siswa) => {
                if (err) {
                    return res.status(500).json({
                        message: "Error when updating siswa.",
                        error: err,
                    });
                }

                return res.json(siswa);
            });
        });
    },

    /**
     * siswaController.remove()
     */
    remove: (req, res) => {
        var id = req.params.id;

        SiswaModel.findByIdAndRemove(id, (err, siswa) => {
            if (err) {
                return res.status(500).json({
                    message: "Error when deleting the siswa.",
                    error: err,
                });
            }

            return res.status(204).json();
        });
    },
};
