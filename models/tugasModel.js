const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const siswaSchema = new Schema({
    siswa: {
        type: Schema.Types.ObjectId,
        ref: "siswa",
    },
    poin: Number,
});

module.exports = mongoose.model("tugas", siswaSchema, "tugas");
