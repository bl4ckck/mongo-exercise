const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const siswaSchema = new Schema({
    nama_depan: String,
    nama_belakang: String,
    foto: String
});

module.exports = mongoose.model("siswa", siswaSchema, "siswa");
